<?php
declare(strict_types=1);

namespace Zlf\AppPaging;

use Exception;

/**
 * 数据分页组件
 * @property-read  int $total 数据总条数;
 * @property-read int $size 数据分页大小;
 * @property-read int $pageCount 数据总页数;
 * @property-read int $page 当前页码;
 * @property-read int $pagesNum 页码个数;
 * @property-read ?int $prev 上一页页码;
 * @property-read ?int $next 下一页页码;
 * @property-read ?int $first 首页;
 * @property-read ?int $last 尾页;
 * @property-read int $start 开始条数;
 * @property-read int $end 结束条数;
 * @property-read int $limit 数据库分页limit;
 * @property-read int $offset 数据库分页offset;
 */
class Paging
{

    /**
     * 数据总条数
     * @var int
     */
    private int $_total;


    /**
     * 分页大小
     * @var int
     */
    private int $_size = 15;


    /**
     * 总页数
     * @var int
     */
    private int $_pageCount;


    /**
     * 当前页码
     * @var int
     */
    private int $_page = 1;


    /**
     * 页码数
     * @var int
     */
    private int $_pagesNum = 8;


    /**
     * 分页初始化分页类
     * @param int $total
     * @param int $pageSize
     */
    public function __construct(int $total = 0, int $pageSize = 15, int $page = 1)
    {
        $this->_total = $total;
        $this->_size = $pageSize;
        $this->_pageCount = (int)ceil($this->_total / $this->_size);//页码数量
        $this->setPage($page ? $page : 1);
    }


    /**
     * 属性获取器
     * @param string $name
     * @return void
     * @throws Exception
     */
    public function __get(string $name)
    {
        $method = 'get' . ucwords($name);
        if (in_array($method, get_class_methods($this))) {
            return call_user_func([$this, $method]);
        }
        $attr = get_object_vars($this);
        if (isset($attr["_$name"])) {
            if($name==='page'){
                return max(1,$attr["_$name"]);
            }
            return $attr["_$name"];
        }
        throw new Exception("未定义的属性 '$name'");
    }


    /**
     * 设置当前页
     * @param int $_page
     * @return $this
     */
    public function setPage(int $_page): static
    {
        if ($_page < 1) {
            $this->_page = 1;
        } elseif ($_page > $this->_pageCount) {
            $this->_page = $this->_pageCount;
        } else {
            $this->_page = $_page;
        }
        return $this;
    }


    /**
     * 设置页码按钮数
     * @param int $num
     * @return $this
     */
    public function setPagesNum(int $num): static
    {
        $this->_pagesNum = $num;
        return $this;
    }

    /**
     * 获取分页页码
     * @return array
     */
    public function getPages(): array
    {
        $btnnum = min($this->_pagesNum, $this->pageCount);
        if ($this->_page === 1) {//处于第一页，那就直接从第一页开始生成页码
            return range(1, $btnnum);
        }
        if ($this->_page === $this->_pageCount) {//最后一页
            return range(max($this->_page - $this->_pagesNum + 1, 1), $this->_page);
        }
        $pageNum = $this->_pagesNum - 1;//出去本页还需要渲染的按钮
        $half = (int)ceil(($this->_pagesNum - 1) / 2);//对半分按钮数量
        $after = range($this->_page + 1, min($this->_page + $half, $this->_pageCount));//当前页之前的按钮数
        $before = range(max($this->_page - $pageNum + count($after), 1), $this->_page - 1);//当前页之后的按钮数
        $pages = [...$before, $this->_page, ...$after];//获取页码数据
        if (count($pages) < $this->_pagesNum) {//分页页码不足
            if ($pages[0] === 1) {//到顶了
                return range(1, min($this->pagesNum, $this->_pageCount));
            }
        }
        return $pages;
    }


    /**
     *  上一页页码
     * @return int|null
     */
    protected function getPrev(): ?int
    {
        if ($this->_page > 1) {
            return $this->_page - 1;
        }
        return null;
    }


    /**
     *  下一页页码
     * @return int|null
     */
    protected function getNext(): ?int
    {
        if ($this->_page < $this->_pageCount) {
            return $this->_page + 1;
        }
        return null;
    }


    /**
     *  首页页码
     * @return int|null
     */
    protected function getFirst(): ?int
    {
        if ($this->_pageCount > 0) {
            return 1;
        }
        return null;
    }


    /**
     *  尾页页码
     * @return int|null
     */
    protected function getLast(): ?int
    {
        if ($this->_pageCount > 0) {
            return $this->_pageCount;
        }
        return null;
    }


    /**
     *  获取limit
     * @return int|null
     */
    protected function getLimit(): ?int
    {
        return $this->_size;
    }


    /**
     *  获取offset
     * @return int|null
     */
    protected function getOffset(): ?int
    {
        return ($this->_page - 1) * $this->_size;
    }


    /**
     * 数据开始的位置
     * @return int
     */
    private function getStart(): int
    {
        return ($this->_page - 1) * $this->_size + 1;
    }


    /**
     * 获取数据结尾
     * @return int
     */
    private function getEnd(): int
    {
        return min($this->_page * $this->_size, $this->_total);
    }


    /**
     * 生成页码链接
     * @param string $baseUrl url地址
     * @param int|null $page 分页页码
     * @param string $pageName 分页参数
     * @return string
     */
    public function generatePageLink(string $baseUrl, ?int $page, string $pageName): string
    {
        if (!$page) {
            return '';
        }
        $baseUrl = urldecode($baseUrl);
        if (!$baseUrl) return "?" . http_build_query([$pageName => $page]);
        $parse = parse_url($baseUrl);
        $query = [];
        if (isset($parse['query']) && $parse['query']) {
            foreach (explode('&', $parse['query']) as $queryItem) {
                $param = explode("=", $queryItem);
                if (count($param) === 2 && strlen($param[0]) > 0 && strlen($param[1]) > 0) {
                    $query[$param[0]] = $param[1];
                }
            }
        }
        if ($page && $page > 0) {
            $query[$pageName] = $page;
        } else {
            unset($query[$pageName]);
        }
        $url = '';
        if (isset($parse['scheme']) && $parse['scheme']) $url .= $parse['scheme'] . '://';
        if (isset($parse['host']) && $parse['host']) $url .= $parse['host'];
        if (isset($parse['port']) && $parse['port']) $url .= ':' . $parse['port'];
        if (isset($parse['path']) && $parse['path']) $url .= $parse['path'];
        $url .= '?' . http_build_query($query);
        if (isset($parse['fragment']) && $parse['fragment']) $url .= "#" . $parse['fragment'];
        return $url;
    }


    /**
     * 返回分页信息
     * @return array
     */
    public function pages(): array
    {
        return [
            'total' => $this->_total,//总条数
            'pageCount' => $this->_pageCount,//总页数
            'start' => $this->start,//按钮数量
            'end' => $this->end,//按钮数量
            'size' => $this->_size,//分页大小
            'page' => $this->_page,//当前页
            'prev' => $this->prev,//上一页
            'next' => $this->next,//下一页
            'first' => $this->first,//首页
            'last' => $this->last,//尾页
            'pages' => $this->getPages(),//页码列表
        ];
    }


    /**
     * 获取分页链接
     * @param string $baseUrl
     * @param string $pageName
     * @return array
     */
    public function links(string $baseUrl, string $pageName = 'page'): array
    {
        $pages = $this->pages();
        $pages['page'] = $this->generatePageLink($baseUrl, $pages['page'], $pageName);
        $pages['prev'] = $this->generatePageLink($baseUrl, $pages['prev'], $pageName);
        $pages['next'] = $this->generatePageLink($baseUrl, $pages['next'], $pageName);
        $pages['first'] = $this->generatePageLink($baseUrl, $pages['first'], $pageName);
        $pages['last'] = $this->generatePageLink($baseUrl, $pages['last'], $pageName);
        foreach ($pages['pages'] as $index => $item) {
            $pages['pages'][$index] = [
                'url' => $this->generatePageLink($baseUrl, $item, $pageName),
                'page' => $item,
                'active' => $this->page === $item
            ];
        }
        return $pages;
    }
}